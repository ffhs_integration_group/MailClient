/**
 * 
 */
package connections;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Testklasse fuer die Email Klasse. Implementiert diverse Methoden um die setter und getter der Email Klasse zu ueberpruefen.
 */
public class EMailTest{
	
	/**
	 * Hilfsmethode, welche ein Email Objekt erstellt und zurueck gibt.
	 * @return Email Objekt
	 */
	public EMail newEmail() {
		return new EMail("test", "test", "test", "test", "test");
	}

	/**
	 * Test Methode fuer die getText Methode der Emailklasse.
	 */
	@Test
	public void testGetText() {
		EMail mail = newEmail();
		assertEquals(mail.getText(), "test");
	}

	/**
	 * Test Methode fuer die setText Methode der Emailklasse. Setzt den Text eines Email Objektes und ueberprueft
	 * dann ob dieser Wert mit dem Sollwert uebereinstimmt.
	 */
	@Test
	public void testSetText() {
		EMail mail = newEmail();
		mail.setText("new test");
		assertEquals(mail.getText(), "new test");
	}

	/**
	 * Test Methode fuer die getSubjekt Methode der Emailklasse.
	 */
	@Test
	public void testGetSubject() {
		EMail mail = newEmail();
		assertEquals(mail.getSubject(), "test");
	}

	/**
	 * Test Methode fuer die setSubject Methode der Emailklasse. Setzt das Subject eines Email Objektes und ueberprueft
	 * dann ob dieser Wert mit dem Sollwert uebereinstimmt.
	 */
	@Test
	public void testSetSubject() {
		EMail mail = newEmail();
		mail.setSubject("new test");
		assertEquals(mail.getSubject(), "new test");
	}

	/**
	 * Test Methode fuer die getTo Methode der Emailklasse.
	 */
	@Test
	public void testGetTo() {
		EMail mail = newEmail();
		assertEquals(mail.getTo(), "test");
	}

	/**
	 * Test Methode fuer die setTo Methode der Emailklasse. Setzt das To Feld eines Email Objektes und ueberprueft
	 * dann ob dieser Wert mit dem Sollwert uebereinstimmt.
	 */
	@Test
	public void testSetTo() {
		EMail mail = newEmail();
		mail.setTo("new test");
		assertEquals(mail.getTo(), "new test");
	}

	/**
	 * Test Methode fuer die getFrom Methode der Emailklasse.
	 */
	@Test
	public void testGetFrom() {
		EMail mail = newEmail();
		assertEquals(mail.getFrom(), "test");
	}

	/**
	 * Test Methode fuer die getDate Methode der Emailklasse.
	 */
	@Test
	public void testGetDate() {
		EMail mail = newEmail();
		assertEquals(mail.getDate(), "test");
	}

	/**
	 * Test Methode fuer die setDate Methode der Emailklasse. Setzt das Datum eines Email Objektes und ueberprueft
	 * dann ob dieser Wert mit dem Sollwert uebereinstimmt.
	 */
	@Test
	public void testSetDate() {
		EMail mail = newEmail();
		mail.setDate("new test");
		assertEquals(mail.getDate(), "new test");
	}

	/**
	 * Test Methode fuer die setFrom Methode der Emailklasse. Setzt das From Feld eines Email Objektes und ueberprueft
	 * dann ob dieser Wert mit dem Sollwert uebereinstimmt.
	 */
	@Test
	public void testSetFrom() {
		EMail mail = newEmail();
		mail.setFrom("new test");
		assertEquals(mail.getFrom(), "new test");
	}

}
