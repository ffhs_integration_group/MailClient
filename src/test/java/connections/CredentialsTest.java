/**
 * 
 */
package connections;

import static org.junit.Assert.*;

import javax.mail.Session;

import org.junit.Test;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Testklasse fuer den Credentials Singleton.
 * 
 */
public class CredentialsTest {

	/**
	 * Ueberprueft ob die getInstance Methode der Credentials klasse auch wirklich ein Objekt dieser Klasse zurueck gibt.
	 */
	@Test
	public void testGetInstance() {
		Credentials creds = Credentials.getInstance();
		assertTrue(creds  instanceof Credentials);
	}
}
