/**
 * 
 */
package connections;

import static org.junit.Assert.*;

import javax.mail.Session;

import org.junit.Test;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Testklasse fuer die EMailSession Klasse.
 *
 */
public class EMailSessionTest {

	/**
	 * Test Methode, welche ueberprueft, ob die getSendSession eine Session zurueck gibt.
	 */
	@Test
	public void testGetSendSession() {
		Session session = EMailSession.getSendSession();
		assertTrue(session  instanceof Session);
	}

	/**
	 * Test Methode, welche die getReceiveSession testet und ueberprueft ob diese Methode eine Session zurueck gibt.
	 */
	@Test
	public void testGetReceiveSession() {
		Session session = EMailSession.getReceiveSession();
		assertTrue(session  instanceof Session);
	}

}
