package connections;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Enum fuer die Werte der EMail Ordner
 * Falls andere Postfächer geladen werden sollen, kann man hier einfach einen neuen Enum eintrag erstellen
 */
public enum EMailFolder {

	INBOX("inbox");

	private String type;

	/**
	 * Simpler privater Konstruktor, initialisiert die type Eigenschaft
	 * @param type Typ des Enums
	 */
	private EMailFolder(String type) {
		this.type = type;
	}

	/**
	 * Simple toString Methode, welcher den Typ des enums
	 */
	@Override
    public String toString() {
        return this.type;
    }

}
