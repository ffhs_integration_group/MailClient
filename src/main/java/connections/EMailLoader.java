package connections;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Yvo B, Giancarlo B
 * Utility Klasse, die Methode enthält mit welchen man aus Files, Arraylists mit Emails darin machen kann
 *
 */
public class EMailLoader {
	
	private EMailLoader() {}

	/**
	 * Methode, welche den Namen des aktuellsten Files für den aktuellen User zurueck gibt
	 * @return
	 */
	private static String getActualFileName(EMailFolder emailFolder) {
		File folder = new File("temp");
		File[] listOfFiles = folder.listFiles();
		// Reverse Sorting um das aktuellste File als erste zu haben
		Arrays.sort(listOfFiles, (a, b) -> Long.compare(b.lastModified(), a.lastModified()));
		for (File file : listOfFiles) {
			Pattern pattern = Pattern.compile("(\\D+)\\d+(\\w+)" + emailFolder.toString() + "\\.ser");
			Matcher matcher = pattern.matcher(file.getName());
			while (matcher.find()) {
				if (matcher.group(1).equals(Credentials.getInstance().getEmail())) {
					return file.getName();
				}
			}
		}
		return "";
	}

	/**
	 * Gibt einen ArrayList mit allen Emails des aktuellen Users zurueck, wenn ein File vorhanden ist
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static ArrayList<EMail> getMailsFromFile(EMailFolder emailFolder) throws IOException, ClassNotFoundException {
		ArrayList<EMail> emails = null;
		String filename = getActualFileName(emailFolder);
		try (FileInputStream fis = new FileInputStream("temp/" + filename);
				ObjectInputStream ois = new ObjectInputStream(fis);) {
			emails = (ArrayList<EMail>) ois.readObject();
		}
		return emails;
	}
}
