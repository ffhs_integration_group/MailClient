package connections;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Klasse, welche EMails empfangen kann.
 */
public class EMailReceiver {

	private Folder folder;
	private Store store;

	/**
	 * Holt die Emails mithilfe von javax Mail vom Server, fuehllt sie in Emails
	 * ab und gibt sie als ArrayList mit Emails zurueck
	 * 
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	public void receiveEmails() throws MessagingException, IOException, IndexOutOfBoundsException {
		Session session = EMailSession.getReceiveSession();
		String host = "imap.gmail.com";
		// Get a store for the POP3S protocol
		store = session.getStore("imaps");
		// Connect to the current host using the specified username and password
		store.connect(host, Credentials.getInstance().getEmail(), Credentials.getInstance().getPassword());
		// Create a Folder object corresponding to the given name
		for (EMailFolder folderName : EMailFolder.values()) {
			folder = store.getFolder("inbox");
			// Open the Folder
			folder.open(Folder.READ_WRITE);
			// Get the messages from the server
			// int numberOfMessages = folder.getMessageCount(); debugin purposes
			Message[] messages = folder.getMessages();
			ArrayList<EMail> emails = fillInEMail(messages);
			saveEmails(emails, folderName.toString());
		}
	}

	/**
	 * Fuellt die Messages welche von javax mail zurueck gegeben werden in
	 * serialisierbare Emailobjekte
	 * 
	 * @param messages
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private ArrayList<EMail> fillInEMail(Message[] messages) throws MessagingException, IOException {
		ArrayList<EMail> emails = new ArrayList<>();

		for(Message message : messages) {
			String date = new SimpleDateFormat("dd/MM/yyyy kk:mm").format(message.getSentDate());
			EMail email = new EMail(message.getRecipients(Message.RecipientType.TO)[0].toString(), 
					message.getFrom()[0].toString(), 
					message.getSubject(), 
					renderMail(message),
					date);
			emails.add(email);
		}
		return emails;
	}

	/**
	 * Speichert die ArrayList mit Emails als Datei ab
	 * 
	 * @param emails
	 */
	private void saveEmails(ArrayList<EMail> emails, String type) throws IndexOutOfBoundsException {
		ObjectOutputStream aus = null;
		String to = emails.get(0).getTo();
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
		try {
			aus = new ObjectOutputStream(new FileOutputStream("temp/" + to + timeStamp + type + ".ser"));
			aus.writeObject(emails);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (aus != null) {
					aus.flush();
					aus.close();
				}
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Schliesst alle Worker(folder, store) die gebraucht werden
	 */
	public void closeWorkers() {
		try {
			if (folder != null) {
				folder.close(true);
			}
			if (store != null) {
				store.close();
			}
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Methode welche das message Objekt in einen gut lesbaren Text rendert. Da der Rueckgabewert von message.getContent()
	 * im MIME Format ist.
	 * @param message Die zu rendernde message
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private String renderMail(Message message) throws MessagingException, IOException {
	    String result = "";
	    if (message.isMimeType("text/plain")) {
	        result = message.getContent().toString();
	    } else if (message.isMimeType("multipart/*")) {
	        MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
	        result = getTextFromMimeMultipart(mimeMultipart);
	    }
	    return result;
	}
	
	/**
	 * Hilfsmethode, welche den Text von einem MIME/Multipart Format extrahiert. Arbeitet Rekursiv.
	 * @param mimeMultipart Den mime/Multipart Teil der Nachricht.
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private String getTextFromMimeMultipart(
	        MimeMultipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/plain")) {
	            result = result + "\n" + bodyPart.getContent();
	            break; 
	        } else if (bodyPart.isMimeType("text/html")) {
	            String html = (String) bodyPart.getContent();
	            result = result + "\n" + html;
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
	        }
	    }
	    return result;
	}
}
