package connections;

import java.util.Properties;

import javax.mail.Session;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Utilityklasse die eine Session zum Senden und eine Session zum Empfangen zurueck gibt
 *
 */
public class EMailSession {
	
	private EMailSession() {
		
	}
    
    /**
     * Gibt eine Session mit Konfigurationen zum Senden von Mails zurueck
     * @return
     */
    public static Session getSendSession() {
    	 // Get system properties
        Properties properties = System.getProperties() ;

        // Setup properties for the mail server
        properties.setProperty( "mail.smtp.auth",            "true"           ) ;
        properties.setProperty( "mail.smtp.starttls.enable", "true"           ) ;
        properties.setProperty( "mail.smtp.host",            "smtp.gmail.com" ) ;
        properties.setProperty( "mail.smtp.port",            "587"            ) ;

        // Get the default Session object
        Session session = Session.getDefaultInstance( properties ) ;
        return session;
    }
    
    /**
     * Gibt eine Session zum Empfangen von Emails zurueck
     * @return
     */
    public static Session getReceiveSession() {
    	// Get system properties
        Properties properties = System.getProperties() ;
        
        // Request POP3S
        properties.put( "mail.store.protocol", "imaps" ) ;
        properties.put("mail.mime.ignoreunknownencoding", "true");

        // Get the default Session object
        Session session = Session.getDefaultInstance( properties ) ;
        
        return session;
    }
}
