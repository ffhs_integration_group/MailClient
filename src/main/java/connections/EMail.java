package connections;

import java.io.Serializable;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin EMail Klasse, welche zum Senden
 *         und Empfangen/Speichern von Emails verwendet wird. Ist Serialisierbar
 *         um die Emails zu speichern.
 * 
 */
public class EMail implements Serializable {

	private static final long serialVersionUID = 1L;
	private String text;
	private String subject;
	private String to;
	private String from;
	private String date;

	/**
	 * Simpler Konstruktor, welche diverse Datenfelder initialisiert
	 * 
	 * @param to
	 *            Empfaengeradresse
	 * @param from
	 *            Senderadresse
	 * @param subject
	 *            Betreff
	 * @param text
	 *            Effektive Nachricht
	 * @param date
	 *            Sendedatum
	 */
	public EMail(String to, String from, String subject, String text, String date) {
		this.text = text;
		this.subject = subject;
		this.to = to;
		this.from = from;
		this.date = date;
	}

	/**
	 * Wird aufgerufen um ein EMail zu verschicken
	 * 
	 * @throws MessagingException
	 */
	public void send() throws MessagingException {
		if (text != "" && subject != "" && to != "") {
			Session session = EMailSession.getSendSession();
			// Create a default MimeMessage object
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header
			message.setFrom(new InternetAddress(Credentials.getInstance().getEmail()));

			// Set To: header field of the header
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// Set Subject: header field
			message.setSubject(subject);

			// Now set the actual message
			message.setText(text);

			Transport transport = session.getTransport("smtps");

			try {
				transport.connect("smtp.gmail.com", Credentials.getInstance().getEmail(),
						Credentials.getInstance().getPassword());
				transport.sendMessage(message, message.getAllRecipients());
			} finally {
				transport.close();
			}
		}
	}

	/**
	 * Getter fuer den Text des Emails
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Setter fuer den Text des Emails
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Getter fuer den Betreff des Emails
	 * 
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Setter fuer den Betreff des EMails
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Getter fuer das To des Emails
	 * 
	 * @return
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Setter fuer das To des Emails
	 * 
	 * @param to
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * Getter fuer das von des Emails
	 * 
	 * @return
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Getter fuer das Datum des Emails
	 * 
	 * @return
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Setter fuer das Datum des Emails
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Setter fuer das von des Emails
	 * 
	 * @param from
	 */
	public void setFrom(String from) {
		this.from = from;
	}

}
