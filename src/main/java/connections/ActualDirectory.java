package connections;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Singletonklasse, welche die Datenfelder email und password enthaelt. Wird beim Login erstellt.
 */
public class ActualDirectory {
	
	private static final ActualDirectory INSTANCE = new ActualDirectory();
	
	private EMailFolder name = EMailFolder.INBOX;
	
	private ActualDirectory() {}
	
	/**
	 * Liefert das Credentialsobjekt
	 * @return Credentials Objekt
	 */
	public static ActualDirectory getInstance() {
		return INSTANCE;
	}
	
	/**
	 * gibt des ausgewaehlten Postfachs zurueck
	 * @return email
	 */
	public EMailFolder getDirectory() {
		return name;
	}
	
	public void setDirectory(EMailFolder folder) {
		this.name = folder;
	}

}
