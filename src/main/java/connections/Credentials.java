package connections;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Singletonklasse, welche die Datenfelder email und password enthaelt. Wird beim Login erstellt.
 */
public class Credentials {
	
	private static final Credentials INSTANCE = new Credentials();
	
	private String email;
	private String password;
	
	private Credentials() {}
	
	/**
	 * Liefert das Credentialsobjekt
	 * @return Credentials Objekt
	 */
	public static Credentials getInstance() {
		return INSTANCE;
	}
	
	/**
	 * gibt die Emailadresse des eingeloggten Users zurueck
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter fuer die Email des Credentialssingletion
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * gibt das Passwort des eingeloggten Users zurueck
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Setter fuer das Passwort des Credentialssingletion
	 * @param email
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
