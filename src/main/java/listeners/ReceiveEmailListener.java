package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import connections.EMailReceiver;
import frames.MainFrame;
import main.MailClientGUI;
import models.EMailTabelModel;

/**
 * Eine Listener KLasse, welche die Funktionalitaet beschreibt, die beim Klicken auf den Empfangen Button ausgefuehrt wird
 * @author Broenniman, Giancarlo Bergamin
 *
 */
public class ReceiveEmailListener implements ActionListener {

	private MainFrame mainFrame;
	
	/**
	 * Simpler Konstruktor, welcher das mainFrame Datenfeld initalisiert.
	 * @param mainFrame Das Haupt Frame
	 */
	public ReceiveEmailListener(MainFrame mainFrame){
		this.mainFrame = mainFrame;
	}
	/**
	 * actionPerformed Methode, welche Emails vom server holt, wenn man auf den Empfangen button klickt. Falls dies
	 * nicht möglich ist (Exception), dann wird eine Statusmeldung in der Statusbar ausgegeben.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		new Thread(new Runnable() {
			public void run() {
				EMailReceiver receiver = new EMailReceiver();
				try {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							mainFrame.getStatusLabel().setText("Try to get emails...");
						}
					});
					receiver.receiveEmails();
					EMailTabelModel tableModel = new EMailTabelModel();
					MainFrame frame = MailClientGUI.getFrame();
					JTable table = frame.getEmailTable();
					table.setModel(tableModel);
					tableModel.fireTableDataChanged();
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							mainFrame.getStatusLabel().setText("Success");
						}
					});
				}catch(MessagingException | IOException | ClassNotFoundException | IndexOutOfBoundsException ex) {
					ex.printStackTrace();
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							mainFrame.getStatusLabel().setText("Couldn't get data from server.");
						}
					});
				}finally {
					receiver.closeWorkers();
				}
			}
		}).start();
	}

}
