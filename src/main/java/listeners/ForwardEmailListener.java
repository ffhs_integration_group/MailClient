package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTable;

import connections.ActualDirectory;
import connections.EMail;
import connections.EMailLoader;
import frames.NewMailFrame;

/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Listenerklasse, welche die Funktionalitaet beschreibt, die ausgefuehrt wird, wenn man aud den Weiterleiten butto klickt.
 */
public class ForwardEmailListener implements ActionListener {

	private JTable emailTable;
	
	/**
	 * Simpler Konstruktor, welcher das emailTable Datenfeld initalisiert
	 * @param emailTable
	 */
	public ForwardEmailListener(JTable emailTable){
		
		this.emailTable = emailTable;
	}
	
	/**
	 * actionPerformed Methode, welche wenn man eine Email in der Mailtabelle ausgewählt hat, ein NewMailFrame erzeugt
	 * und den Betreff und die Nachricht der ausgewaehlten Mail in die entsprechenden Felder dieses Frames fuellt
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		int rowindex = emailTable.getSelectedRow();
		if (rowindex >= 0)	//checks if something is selected (rowindex = -1 if nothing is selected)
		{
			NewMailFrame mailFrame = new NewMailFrame();
			String betreff = (String) emailTable.getModel().getValueAt(rowindex, 2);
			mailFrame.setBetreffField("FWD: " + betreff);			
			try{
				ArrayList<EMail> emails = EMailLoader.getMailsFromFile(ActualDirectory.getInstance().getDirectory());
				mailFrame.setEmailTextPane(emails.get(rowindex).getText());
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		
	}

}
