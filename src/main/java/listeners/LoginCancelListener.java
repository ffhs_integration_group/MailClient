package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Listener Klasse, welche die Funktionalitaet beschreibt, die ausgefuehrt wird, wenn man das Login abbgricht.
 */
public class LoginCancelListener implements ActionListener {

	/**
	 * actionPerformed Methode, welche das Programm beendet, falls der Cancel Button im Login Frame gedrueckt wird
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}

}
