package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.mail.MessagingException;
import javax.swing.SwingUtilities;

import connections.Credentials;
import connections.EMail;
import frames.NewMailFrame;
import main.MailClientGUI;

/**
 * Listener Klasse, welche die Funktionalitaet beschreibt, die ausgefuehrt wird,
 * wenn man auf den Senden Button klickt
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 *
 */
public class SendEmailListener implements ActionListener {

	NewMailFrame frame;

	/**
	 * Simpler Konstruktor, welcher das NewMailFrame Datenfeld initalisiert
	 * 
	 * @param frame
	 */
	public SendEmailListener(NewMailFrame frame) {
		this.frame = frame;
	}

	/**
	 * actionPerformed Methode, welche wenn man auf den senden Button klickt,
	 * die Eingabefelder einem Email Object uebergibt und dann darauf die send
	 * Methode aufruft.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String to = frame.getAnField().getText();
		String subject = frame.getBetreffField().getText();
		String text = frame.getEmailTextPane().getText();
		String from = Credentials.getInstance().getEmail();

		new Thread(new Runnable() {
			public void run() {
				try {
					EMail mail = new EMail(to, from, subject, text, null);
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							MailClientGUI.getFrame().getStatusLabel().setText("Sending Email..");
							frame.dispose();
						}
					});
					mail.send();
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							MailClientGUI.getFrame().getStatusLabel().setText("Email to " + to + " sent");
						}
					});
				} catch (MessagingException ex) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							MailClientGUI.getFrame().getStatusLabel().setText("Cannot send email to: " + to);
						}
					});
				}
			}
		}).start();
	}

}
