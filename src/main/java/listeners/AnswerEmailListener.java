package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;

import frames.NewMailFrame;
/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Listenerklasse, welche dem Answer Button mitgegeben wird, damit der auf events horchen kann. 
 */
public class AnswerEmailListener implements ActionListener {

	private JTable emailTable;
	
	/**
	 * Simpler Konstruktor, welcher das emailTable Datenfeld initialisiert.
	 * @param emailTable Die Emailtabelle, in der alle Emails angezeigt werden
	 */
	public AnswerEmailListener(JTable emailTable){
		
		this.emailTable = emailTable;
	}
	
	/**
	 * actionPerformed Methode. Sofern ein email aus der Tabelle ausgewaehlt ist und man den Answer Button drueckt
	 * wird ein NewMailFrame erzeugt und es werden das "An Feld" und der Betreff mit dem Zusatz "RE" ausgefuellt.
	 * Wir haben bewusst die Email, auf die man antworten will, nicht nochmal eingefuegt, da wir das als stoerend emfunden haben
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		int rowindex = emailTable.getSelectedRow();
		if (rowindex >= 0)	//checks if something is selected (rowindex = -1 if nothing is selected)
		{
			NewMailFrame mailFrame = new NewMailFrame();
			String anAddress = (String) emailTable.getModel().getValueAt(rowindex, 0);
			mailFrame.setAnField(anAddress);
			
			String betreff = (String) emailTable.getModel().getValueAt(rowindex, 2);
			mailFrame.setBetreffField("RE: " + betreff);
		}
		
		
	}



}
