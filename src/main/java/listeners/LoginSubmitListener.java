package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import connections.Credentials;
import connections.EMailReceiver;
import frames.LoginFrame;
import frames.MainFrame;
import main.MailClientGUI;
import models.EMailTabelModel;

/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Listener Klasse, welche die Funktionalitaet beschreibt, die ausgefuehrt wird, wenn man den Login SUbmit Button betaetigt
 */
public class LoginSubmitListener implements ActionListener {

	LoginFrame loginFrame;

	/**
	 * Simpler Konstruktor, welcher das loginFrame Datenfeld initalisiert
	 * @param loginFrame Das Login Frame
	 */
	public LoginSubmitListener(LoginFrame loginFrame) {
		this.loginFrame = loginFrame;
	}

	/**
	 * actionPerformed Methode, welche beim betatigen des Submit Buttons die Credentials in den Singleton schreibt
	 * und dann versucht Emails von einem bestehenden File zu laden. Falls dies nicht gelingt wir die entsprechende
	 * Fehlermeldung in die Statusbar geschrieben und es wird versucht die Mails vom server zu holen. Falls dies auch nicht
	 * gelingt, wird wieder eine Fehlermeldung ausgegeben (Statusbar). Man kann dann versuchen sich weiter einzuloggen.
	 */
	@Override
	public void actionPerformed(ActionEvent a) {
		String email = loginFrame.getEmailField().getText();
		String password = loginFrame.getPasswordField().getText();
		try {
			Credentials.getInstance().setEmail(email);
			Credentials.getInstance().setPassword(password);
			loadEmailsFromFile();
			loginFrame.dispose();
		} catch (IOException | ClassNotFoundException ex) {
			new Thread(new Runnable() {
				public void run() {
					try {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								loginFrame.getStatusLabel().setText("Try to get emails");
							}
						});
						loadEmailsFromServer();
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								loginFrame.dispose();
							}
						});
					} catch (IOException | MessagingException | ClassNotFoundException e) {
						e.printStackTrace();
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								e.printStackTrace();
								loginFrame.getStatusLabel().setText("Couldn't get data from server, credentials may be wrong");
							}
						});
					}
				}
			}).start();
		}
	}

	/**
	 * Methode, welche versucht Emails von einem bestehenden File auf dem Filesystem zu laden.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void loadEmailsFromFile() throws IOException, ClassNotFoundException {
		EMailTabelModel tableModel = new EMailTabelModel();
		setModelToTable(tableModel);
	}

	/**
	 * Methode, welche versucht Emails vom server zu laden.
	 * @throws MessagingException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void loadEmailsFromServer() throws MessagingException, IOException, ClassNotFoundException {
		EMailReceiver receiver = new EMailReceiver();
		receiver.receiveEmails();
		EMailTabelModel tableModel = new EMailTabelModel();
		setModelToTable(tableModel);
	}

	/**
	 * Methode, welche das TableModel anpasst.
	 * @param tableModel 
	 */
	private void setModelToTable(EMailTabelModel tableModel) {
		MainFrame frame = MailClientGUI.getFrame();
		JTable table = frame.getEmailTable();
		table.setModel(tableModel);
		tableModel.fireTableDataChanged();
	}
}
