package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frames.NewMailFrame;

/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Listenerklasse, welche eine Methode zum schliessen eines neuen MailFrames bereitstellt. 
 */
public class CancelEmailListener implements ActionListener {

	NewMailFrame frame;
	
	/**
	 * Konstruktor, welche das Datenfeld frame initialisiert
	 * @param frame Ein Objekt der Klass NewMailFrame
	 */
	public CancelEmailListener(NewMailFrame frame) {
		this.frame = frame;
	}
	
	/**
	 * actionPerformed Methode, welche das NewMailFrame schliesst (wird aufgerufen, wenn man auf den cancel button drueckt)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		frame.dispose();
	}

}
