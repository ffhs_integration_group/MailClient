package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frames.NewMailFrame;

/**
 * 
 * @author Broenniman, Giancarlo Bergamin
 * Eine Listener Klasse, welche dei Funktionalitaet beschreibt, die ausgefuehrt wird, wenn man auf den Neue Email Button klickt
 */
public class NewEmailListener implements ActionListener {

	/**
	 * actionPerformed Methode, welche einen NewMailFrame erzeugt, wenn man auf den Neue Email button klickt
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		new NewMailFrame();
	}

}
