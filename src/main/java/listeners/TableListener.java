package listeners;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import connections.ActualDirectory;
import connections.EMail;
import connections.EMailLoader;

/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Ein Listener, welcher eine valueChanged Methode implementiert. Diese Klasse implementiert das ListSelectionListener interface.
 * 
 *
 */
public class TableListener implements ListSelectionListener {

	private JTable emailTable;
	private JTextArea mailArea;
	
	/**
	 * Simpler Konstruktor, welcher die beiden Datenfelder initialisiert
	 * @param emailTable	Die Emailtabelle des Hauptfensters.
	 * @param mailArea	Die JTextArea des Hautfensters.
	 */
	public TableListener(JTable emailTable, JTextArea mailArea){
		
		this.emailTable = emailTable;
		this.mailArea = mailArea;
	}
	/**
	 * Simpler Konstruktor, welcher das JTable Datenfeld initialisiert 
	 * @param emailTable	Die Emailtabelle des Hauptfensters.
	 */
	public TableListener(JTable emailTable){
		
		this.emailTable = emailTable;
	}
	
	/**
	 * valueChanged Methode (vom Interface vorrausgesetzt). Immer wenn eine Zeile in der Emailtabelle angewählt wird,
	 * wird das entsprechende Mail geladen und auf der TextAre im unteren rechten Bildschirmrand angezeigt.
	 */
	@Override
	public void valueChanged(ListSelectionEvent e){
		int rowindex = emailTable.getSelectedRow();
		try{
			ArrayList<EMail> emails = EMailLoader.getMailsFromFile(ActualDirectory.getInstance().getDirectory());
			mailArea.setText(emails.get(rowindex).getText()); 	// Holt den Text des ausgewaehlten Emails und setzt ihn in das JTextArea im Hauptfenster 
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}
	
	/**
	 * getter fuer die gerade selektierte Zeile in der Emailtabelle.
	 * @return Den Index der ausgewählten Zeile
	 */
	public int getRowIndex(){
		
		return emailTable.getSelectedRow();
	}

}
