package models;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import connections.ActualDirectory;
import connections.EMail;
import connections.EMailLoader;

/**
 * Das Table Model fuer die EmailTabelle.
 * @author Yvo Broenniman, Giancarlo Bergamin
 *
 */
public class EMailTabelModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	public static ArrayList<String[]> rows;
	private static final String[] NAMES = { "Absender", "Zeit", "Betreff" };

	/**
	 * Konstruktor welcher die Email laed und dises dann in die Tabelle abfuellt
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public EMailTabelModel() throws IOException, ClassNotFoundException {
		ArrayList<EMail> emails = EMailLoader.getMailsFromFile(ActualDirectory.getInstance().getDirectory());
		fillInTable(emails);
	}

	/**
	 * Diese Methode fuellt die Emails in einer ArrayList in die Tabelle ab.
	 * @param emails Eine ArrayList von EMail Objekten
	 */
	public void fillInTable(ArrayList<EMail> emails) {
		rows = new ArrayList<>();
		try {
			for (EMail email : emails) {
				rows.add(new String[] { email.getFrom(), email.getDate(), email.getSubject() });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * getter fuer die Spaltenueberschriften
	 */
	@Override
	public String getColumnName(int i) {
		return NAMES[i];

	}

	/**
	 * getter fuer die Anzahl Zeilen
	 */
	@Override
	public int getRowCount() {
		return rows.size();
	}

	/**
	 * getter fuer die Anzahl Spalten
	 */
	@Override
	public int getColumnCount() {
		String[] row = rows.get(0);
		return row.length;
	}
	
	/**
	 * getter fuer einen bestimmten Wert in einer Tabellenzelle.
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String[] row = rows.get(rowIndex);
		return row[columnIndex];
	}

}
