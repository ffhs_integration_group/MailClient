package models;

import java.io.IOException;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import connections.ActualDirectory;
import connections.EMailFolder;
import main.MailClientGUI;

public class EMailDirectoryTree {

	public static JTree getEMailTree() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Directories");
		for (EMailFolder folderType : EMailFolder.values()) {
			root.add(new DefaultMutableTreeNode(folderType.name()));
		}
		JTree tree = new JTree(root);
		tree.addTreeSelectionListener(new TreeSelectionListener() {
		    public void valueChanged(TreeSelectionEvent e) {
		    	JTree tree = (JTree) e.getSource();
		        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree
		            .getLastSelectedPathComponent();
		        String selectedNodeName = selectedNode.toString();
		        if (selectedNode.isLeaf()) {
		        	ActualDirectory.getInstance().setDirectory(EMailFolder.valueOf(selectedNodeName));
		        	EMailTabelModel tableModel;
					try {
						tableModel = new EMailTabelModel();
						MailClientGUI.getFrame().getEmailTable().setModel(tableModel);
					} catch (ClassNotFoundException | IOException e1) {
						e1.printStackTrace();
					}
		        	
		        }
		    }
		});
		return tree;
	}

}
