package main;

import frames.LoginFrame;
import frames.MainFrame;

/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Main Klasse, mit main method welche das MainFrame und das LoginFrame aufruft.
 *
 */
public class MailClientGUI {
	
	static MainFrame frame = new MainFrame();
	
	/**
	 * main Methode, welche das LoginFrame (und auch das MainFrame) aufruft beim starten des Clients.
	 * @param args Keine Argumente benoetigt.
	 */
	public static void main(final String[] args)
	{
		new LoginFrame(frame, true); //Dialog muss parent übergeben werden um zu blockieren, true sagt, dass er blockieren soll
	}	
	
	/**
	 * getter fuer das MainFrame
	 * @return Das MainFrame
	 */
	public static MainFrame getFrame() {
		return frame;
	}

}
