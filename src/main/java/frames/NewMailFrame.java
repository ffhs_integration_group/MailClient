package frames;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;

import listeners.CancelEmailListener;
import listeners.SendEmailListener;
/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Klasse, welche ein neues Email Frame erstellt (Ein frame, in dem mann eine neue Email schreiben kann). 
 */
public class NewMailFrame extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	private JTextField anField;
	private JTextField betreffField;
	private JTextPane emailTextPane;
	
	/**
	 * Konstruktor, welcher zwei Hilfmethoden aufruft, um ein newEmailFrame zu erstellen.
	 */
	public NewMailFrame() {
		super("New Mail");
		
		add(createToolBarPanel(), BorderLayout.NORTH);
		add(createMainPanel(), BorderLayout.CENTER);
		
		setSize(700, 700);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}
	
	/**
	 * Erstellt die Toolbar fuer das newEmailFrame (mit Buttons fuer das versenden von emails etc.)
	 * @return JPanel toolBar
	 */
	private JPanel createToolBarPanel() {
		final JToolBar toolBar = new JToolBar();
		JButton sendButton = new JButton("Senden");
		sendButton.addActionListener(new SendEmailListener(this));
		JButton cancelButton = new JButton("Abbrechen");
		cancelButton.addActionListener(new CancelEmailListener(this));
		toolBar.add(sendButton);
		toolBar.add(cancelButton);

		final JPanel toolbarPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		toolbarPanel.add(toolBar);
		
		return toolbarPanel;
	}
	
	/**
	 * Hilfsmethode fuer die Erstellung des Hauptpanels.
	 * @return JSpiltPane pane
	 */
	private JSplitPane createMainPanel() {
		JSplitPane pane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		
		pane.setTopComponent(createInformationPanel());
		pane.setBottomComponent(createTextPane());
				
		return pane;
	}
	
	/**
	 * Hilfsmethode welche den Informationpart des panels erzeugt, sprich Textfelder, Labels etc.
	 * @return JPanel emailInformation
	 */
	private JPanel createInformationPanel() {
		JPanel emailInformation = new JPanel();
		JLabel an = new JLabel("An:");
		JLabel betreff = new JLabel("Betreff:");
		anField = new JTextField(15);
		betreffField  = new JTextField(15);
		emailInformation.setLayout(new GridLayout(2,2));
		emailInformation.add(an);
		emailInformation.add(anField);
		emailInformation.add(betreff);
		emailInformation.add(betreffField);
		
		return emailInformation;
	}
	
	/**
	 * Hilfsmethode, welche die TextPane für den Inhalt des Mails erzeugt.
	 * @return JTextPane emailTextPane
	 */
	private JTextPane createTextPane() {
		
		emailTextPane = new JTextPane();
		emailTextPane.setPreferredSize(getPreferredSize());
		emailTextPane.setEditable(true);
		return emailTextPane;
	}
	
	/**
	 * getter fuer das An Feld
	 * @return An Feld
	 */
	public JTextField getAnField() {
		return anField;
	}

	/**
	 * getter fuer das Betreff Feld
	 * @return Betreff Feld
	 */
	public JTextField getBetreffField() {
		return betreffField;
	}

	/**
	 * getter fuer die TextPane des Email contents
	 * @return
	 */
	public JTextPane getEmailTextPane() {
		return emailTextPane;
	}

	/**
	 * setter fuer das an Feld
	 * @param mailAddress Mailadresse des Empfaengers
	 */
	public void setAnField(String mailAddress){
		anField.setText(mailAddress);
	}
	
	/**
	 * setter fuer das Betreff Feld
	 * @param betreff Betreff des Mails
	 */
	public void setBetreffField(String betreff) {
		betreffField.setText(betreff);
	}
	
	/**
	 * setter für das Content Text Feld der Email
	 * @param text Text des Mails
	 */
	public void setEmailTextPane(String text){
		emailTextPane.setText(text);
		
	}
}
