package frames;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.JTree;

import listeners.AnswerEmailListener;
import listeners.ForwardEmailListener;
import listeners.NewEmailListener;
import listeners.ReceiveEmailListener;
import listeners.TableListener;
import models.EMailDirectoryTree;

/**
 * 
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Klasse, welche das MainFrame (das Frame in dem die Emails angezeigt werden etc.) erstellt und den verschiednen
 * Benutzeroberflaechen Listener zuweist, damit gewuenschte Funktionalitaet erlangt wird. 
 */
public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	JTable emailTable;
	JTextArea mailArea;
	private JLabel statusLabel;

	/**
	 * Konstruktor, in welchem diverse Hilfsmethoden für die Erstellung der einzelnen Framkomponenten aufgerufen werden.
	 * 
	 */
	public MainFrame() {
		super("Mail Client");
		add(createMainSection(), BorderLayout.CENTER);
		add(createToolBarPanel(), BorderLayout.NORTH);
		add(createStatusBarPanel(), BorderLayout.SOUTH);

		setSize(900, 900);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * Methode, welche die ToolBar am oberen Rand des MainFrames erstellt. Es werden zu den verschiedenen Buttons verschiedene
	 * Listener hinzugefuegt, damit die gewuenschte Funktionalitaet hinzugefuegt wird. 
	 * @return
	 */
	private JPanel createToolBarPanel() {
		final JToolBar toolBar = new JToolBar();
		
		JButton newEmailButton = new JButton("Neue Email");
		newEmailButton.addActionListener(new NewEmailListener());
		toolBar.add(newEmailButton);
		
		JButton receiveEmailButton = new JButton("Empfangen");
		receiveEmailButton.addActionListener(new ReceiveEmailListener(this));
		toolBar.add(receiveEmailButton);
				
		JButton answerEmailButton = new JButton("Antworten");
		answerEmailButton.addActionListener(new AnswerEmailListener(getEmailTable()));
		toolBar.add(answerEmailButton);

		JButton forwardEmailButton = new JButton("Weiterleiten");
		forwardEmailButton.addActionListener(new ForwardEmailListener(getEmailTable()));
		toolBar.add(forwardEmailButton);


		final JPanel toolbarPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		toolbarPanel.add(toolBar);

		return toolbarPanel;
	}

	/**
	 * Hilfsmethode, welche die MainSection (Email Tabelle, Anzeigeflaeche etc.) erstellt.
	 * @return JComponent Die MainSection.
	 */
	private JComponent createMainSection() {

		final JTree tree = EMailDirectoryTree.getEMailTree();
		emailTable = new JTable();

		final JScrollPane mailList = new JScrollPane(emailTable);
		JTextArea mailArea = new JTextArea();
		mailArea.setEditable(false);
        mailArea.setMargin(new Insets(10,10,10,10));
		final JScrollPane mail = new JScrollPane(mailArea);
		
		emailTable.getSelectionModel().addListSelectionListener(new TableListener(emailTable, mailArea));
			
		final JSplitPane splitPane = new JSplitPane();
		final JSplitPane mailSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		mailSplitPane.setTopComponent(mailList);
		mailSplitPane.setBottomComponent(mail);
		splitPane.setLeftComponent(new JScrollPane(tree));
		splitPane.setRightComponent(mailSplitPane);

		return splitPane;
		
		
	}

	/**
	 * Erzeugt die Statusbar am unteren Rand des MainFrames
	 * @return JPanel StatusBar
	 */
	private JPanel createStatusBarPanel() {
		final JToolBar statusBar = new JToolBar();

		statusLabel = new JLabel("");
		final JPanel statusbarPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		statusbarPanel.add(statusBar);
		statusBar.add(statusLabel);

		return statusbarPanel;
	}
	
	/**
	 * getter fuer das Statuslabel
	 * @return JLabel statusLabel
	 */
	public JLabel getStatusLabel() {
		return statusLabel;
	}
	
	/**
	 * setter fuer das Statuslabel
	 * 
	 * @param statusLabel Ein zu setzendes statusLabel
	 */
	public void setStatusLabel(JLabel statusLabel) {
		this.statusLabel = statusLabel;
	}
	
	/**
	 * getter für die Emailtabelle.
	 * @return JTable emailTable
	 */
	public JTable getEmailTable() {
		return emailTable;
	}
}
