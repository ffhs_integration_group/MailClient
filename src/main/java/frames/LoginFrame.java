package frames;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JToolBar;
/**
 * @author Yvo Broenniman, Giancarlo Bergamin
 * Eine Klasse für das Loginframe, in welchem man seine Credentials eingibt.
 *
 */
public class LoginFrame extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private JTextField emailField;
	private JPasswordField passwordField;
	private JLabel statusLabel;

	/**
	 * 
	 * @param parent Parent, welcher uebergeben wird, um diesen zu blockieren
	 * @param modal Boolean, welcher besagt ob Parent blockiert werden soll
	 */
	public LoginFrame(Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("Login");
		add(createMainPanel(), BorderLayout.CENTER);
		add(createStatusBarPanel(), BorderLayout.SOUTH);
		setSize(450, 155);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.MODELESS);
	}
	
	/**
	 * Methode, welche das MainLoginPanel erstellt.
	 * @return Ein panel (Login)
	 */
	private JPanel createMainPanel() {
		JLabel emailLabel = new JLabel("email:");
		emailField = new JTextField(15);
		JLabel passwordLabel = new JLabel("password:");
		passwordField = new JPasswordField(15);
		JPanel panel = new JPanel(new GridLayout(3,2));
		JButton submit = new JButton("submit");
		submit.addActionListener(new listeners.LoginSubmitListener(this));
		JButton cancel = new JButton("cancel");
		cancel.addActionListener(new listeners.LoginCancelListener());
		panel.add(emailLabel);
		panel.add(emailField);
		panel.add(passwordLabel);
		panel.add(passwordField);
		panel.add(cancel);
		panel.add(submit);
		return panel;
	}
	
	/**
	 * Eine Hilfsmethode, welche die Statusbar am unteren Rand des LoginFrames erzeugt.
	 * @return statusbarPanel
	 */
	private JPanel createStatusBarPanel() {
		final JToolBar statusBar = new JToolBar();
	    statusLabel = new JLabel("");
		final JPanel statusbarPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		statusbarPanel.add(statusBar);
		statusBar.add(statusLabel);

		return statusbarPanel;
	}
	
	/**
	 * getter fuer das email Textfeld
	 * @return JTextField emailField
	 */
	public JTextField getEmailField() {
		return emailField;
	}
	
	/**
	 * getter fuer das password Textfeld
	 * @return JTextField passwordField
	 */
	public JTextField getPasswordField() {
		return passwordField;
	}
	
	/**
	 * getter fuer das Statuslabel
	 * @return JLabel statusLabel
	 */
	public JLabel getStatusLabel() {
		return statusLabel;
	}

	/**
	 * setter fuer das Statuslabel
	 * 
	 * @param statusLabel Ein zu setzendes statusLabel
	 */
	public void setStatusLabel(JLabel statusLabel) {
		this.statusLabel = statusLabel;
	}

}
